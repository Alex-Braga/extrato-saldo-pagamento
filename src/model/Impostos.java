package model;

import interfaces.TipoImposto;


public class Impostos extends Boleto implements TipoImposto{
	
	private TipoImposto tipoImposto;
	
	public Impostos(int id_boleto, String cod_barras, double valor, String data_vencimento, String data_pagamento,
			String hora_pagamento, String beneficiario, String servico, String CNPJ, String agencia, String conta,
			String operacao, double taxaJuros) {
		super(id_boleto, cod_barras, valor, data_vencimento, data_pagamento, hora_pagamento, beneficiario, servico, CNPJ,
				agencia, conta, operacao);
		// TODO Auto-generated constructor stub
	}
	
	public Impostos() {}
	
	
	public TipoImposto getTipoImposto() {
		return tipoImposto;
	}

	public void setTipoImposto(TipoImposto tipoImposto) {
		this.tipoImposto = tipoImposto;
	}

}
