package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import model.Boleto;

class BoletoControllerTest {
 
	@Test
	void buscarBoletoPeloCodBarras() {
		Boleto boleto = new Boleto();
		BoletoController boletoController = new BoletoController();
		boletoController.boleto = boleto;
		boletoController.boleto.setCod_barras("555555555");
		boletoController.buscarBoleto(boletoController.boleto.getCod_barras());
		Assert.assertNotNull(boletoController.boleto);
	}

}
