/**
 * 
 */
package model;

/**
 * @author dan
 *
 */
public class Extrato {
	
	private String idTransacao;
	private String dataTransacao;
	private String horaTransacao;
	private String cpfOuCnpjDoClienteTitular;
	private String descricaoTransacao;
	private String valorTransacao;
	private String statusTransacao;
	private String tipoTransacao;

	


	public String getCpfOuCnpjDoClienteTitular() {
		return cpfOuCnpjDoClienteTitular;
	}

	public void setCpfOuCnpjDoClienteTitular(String cpfOuCnpjDoClienteTitular) {
		this.cpfOuCnpjDoClienteTitular = cpfOuCnpjDoClienteTitular;
	}

	/**
	 * @return the idTransacao
	 */
	public String getIdTransacao() {
		return idTransacao;
	}
	
	/**
	 * @param idTransacao the idTransacao to set
	 */
	public void setIdTransacao(String idTransacao) {
		this.idTransacao = idTransacao;
	}
	
	/**
	 * @return the dataTransacao
	 */
	public String getDataTransacao() {
		return dataTransacao;
	}
	
	/**
	 * @param dataTransacao the dataTransacao to set
	 */
	public void setDataTransacao(String dataTransacao) {
		this.dataTransacao = dataTransacao;
	}
	
	/**
	 * @return the horaTransacao
	 */
	public String getHoraTransacao() {
		return horaTransacao;
	}
	
	/**
	 * @param horaTransacao the horaTransacao to set
	 */
	public void setHoraTransacao(String horaTransacao) {
		this.horaTransacao = horaTransacao;
	}
	
	/**
	 * @return the descricaoTransacao
	 */
	public String getDescricaoTransacao() {
		return descricaoTransacao;
	}
	
	/**
	 * @param descricaoTransacao the descricaoTransacao to set
	 */
	public void setDescricaoTransacao(String descricaoTransacao) {
		this.descricaoTransacao = descricaoTransacao;
	}
	
	/**
	 * @return the valorTransacao
	 */
	public String getValorTransacao() {
		return valorTransacao;
	}
	
	/**
	 * @param valorTransacao the valorTransacao to set
	 */
	public void setValorTransacao(String valorTransacao) {
		this.valorTransacao = valorTransacao;
	}
	
	/**
	 * @return the statusTransacao
	 */
	public String getStatusTransacao() {
		return statusTransacao;
	}
	
	/**
	 * @param statusTransacao the statusTransacao to set
	 */
	public void setStatusTransacao(String statusTransacao) {
		this.statusTransacao = statusTransacao;
	}
	
	/**
	 * @return the tipoTransacao
	 */
	public String getTipoTransacao() {
		return tipoTransacao;
	}
	
	/**
	 * @param tipoTransacao the tipoTransacao to set
	 */
	public void setTipoTransacao(String tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}
	
	public Extrato() {
		
	}

}
