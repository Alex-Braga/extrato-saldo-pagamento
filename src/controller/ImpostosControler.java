package controller;

import interfaces.BuscarBoleto;
import model.Boleto;
import model.Impostos;

public class ImpostosControler implements BuscarBoleto{
	Impostos impostos;
	
	@Override
	public Boleto buscarBoleto(String cod_barras) {
		if (impostos.getCod_barras() == cod_barras) return impostos; 
		else return null;
	}

}
