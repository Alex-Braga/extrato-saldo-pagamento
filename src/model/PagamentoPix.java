package model;

import java.time.LocalDateTime;

public class PagamentoPix extends Pagamento{
	
	//Chaves PIX
	private String chaveCliente;
	private String chaveAleatoria;
	private String chaveTelefone;
	private String chaveEmail;
	private String chaveCpf;
	private String chaveCnpj; 
	
	
	public PagamentoPix(int id, LocalDateTime data, String nomePagador, String cpfOuCnpjPagador,
						String nomeRecebedor,String cpfOuCnpjRecebedor, double valor, String data_pagamento, 
						String hora_pagamento,String autenticacao, String chaveAleatoria, String chaveTelefone,
						String chaveEmail, String chaveCpf, String chaveCnpj) {
		
		super(id, data, nomePagador, cpfOuCnpjPagador, nomeRecebedor,
			  cpfOuCnpjRecebedor, valor, data_pagamento, hora_pagamento,autenticacao);
		// TODO Auto-generated constructor stub
	}
	
	
	public String getChaveAleatoria() {
		return chaveAleatoria;
	}

	public void setChaveAleatoria(String chaveAleatoria) {
		this.chaveAleatoria = chaveAleatoria;
	}

	public String getChaveTelefone() {
		return chaveTelefone;
	}

	public void setChaveTelefone(String chaveTelefone) {
		this.chaveTelefone = chaveTelefone;
	}

	public String getChaveEmail() {
		return chaveEmail;
	}

	public void setChaveEmail(String chaveEmail) {
		this.chaveEmail = chaveEmail;
	}

	public String getChaveCpf() {
		return chaveCpf;
	}

	public void setChaveCpf(String chaveCpf) {
		this.chaveCpf = chaveCpf;
	}

	public String getChaveCnpj() {
		return chaveCnpj;
	}

	public void setChaveCnpj(String chaveCnpj) {
		this.chaveCnpj = chaveCnpj;
	}
	public String getChaveCliente() {
		return chaveCliente;
	}


	public void setChaveCliente(String chaveCliente) {
		this.chaveCliente = chaveCliente;
	}

	
	public PagamentoPix() {}


	
}