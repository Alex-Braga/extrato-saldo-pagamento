package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import interfaces.TipoImposto;
import model.Boleto;
import model.ClientePF;
import model.Extrato;
import model.Impostos;
import model.PagamentoBoleto;
import model.PagamentoImpostos;

class ExtratoControllerTest {

	@Test
	void lancarPagamentoBoletoNoExtrato() {
		PagamentoBoleto pagamentoBoleto = new PagamentoBoleto();
		PagamentoBoletoController pagamentoBoletoController = new PagamentoBoletoController();
		
		Extrato extrato = new Extrato();
		ExtratoController extratoControler = new ExtratoController();
		
		extratoControler.extrato = extrato;
		
		pagamentoBoletoController.pagamentoBoleto = pagamentoBoleto;
		
		ClientePF cliente = new ClientePF(2, "Diego", "12199022100", 2000, "12199022100");
		Boleto boleto = new Boleto(90, "55555555555", 1000, "27/06/2021", "26/06/2021", "10:00", "Kabum", 
								   "Compra Online", "1091892000199", "12311", "10928881-1", "pag-boleto");
		
		
		pagamentoBoletoController.salvarPagamentoDeBoleto(boleto, cliente);
		
		extratoControler.lancarPagamentoBoleto(pagamentoBoletoController.pagamentoBoleto);
		Assert.assertNotNull(pagamentoBoletoController.pagamentoBoleto);
		Assert.assertNotNull(extratoControler.listarPagamentosBoleto());
	
	}
	
	
	
	@Test
	void lancarPagamentoImpostos() {
		PagamentoImpostos pagamentoImpostos = new PagamentoImpostos();
		PagamentoImpostosController controler = new PagamentoImpostosController();
		
		Extrato extrato = new Extrato();
		ExtratoController extratoControler = new ExtratoController();
		
		extratoControler.extrato = extrato;
		
		controler.pagamentoImpostos = pagamentoImpostos;
		
		//TipoImposto imposto;
		
		ClientePF cliente = new ClientePF(2, "Diego", "12199022100", 2000, "12199022100");
		Impostos impostos = new Impostos(70, "8888888888888", 1900, "26/06/2021", "01/09/2021", 
				"16:00", "Alex", "IPTU", "1091892000199", "45789", "202156-7", "pag-imposto", TipoImposto.IPTU );
		
		
		controler.salvarPagamentoImposto(impostos, cliente);
		
		extratoControler.lancarPagamentoImpostos(controler.pagamentoImpostos);
		Assert.assertNotNull(controler.pagamentoImpostos);
		Assert.assertNotNull(extratoControler.listarPagamentosImpostos());
	}
	
	@Test
	void lancarPagamentoPix() {
		
	}
	

}
