package controller;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import interfaces.BuscarBoleto;
import interfaces.TipoCliente;
import interfaces.TipoImposto;
import interfaces.TituloPagamento;
import model.Boleto;
import model.ClientePF;
import model.Impostos;
import model.PagamentoBoleto;
import model.PagamentoImpostos;

public class PagamentoImpostosController{

	PagamentoImpostos pagamentoImpostos; //Model

	
	
	/*public PagamentoImpostosController(PagamentoImpostos pagamentoImpostos){
		this.pagamentoImpostos = pagamentoImpostos;
	}*/
	
	
	
	/**
	 * Metodo que verifica se o tributo � menor do que o saldo em conta;
	 * 
	 * @param saldoConta
	 * @param valorTributo
	 * @return
	 */
	public double confirmarPagamento(double saldoConta, double valorTributo) {
		if(saldoConta >= valorTributo) {
			double verificacaoSaldo = saldoConta - valorTributo;
			return verificacaoSaldo;
		}
		return 0;	
	}
	
	/**
	 * m�todo para receber informa��es do boleto e informa��es do cliente
	 * os dados do pagamento somente ser�o salvos se o saldo for maior que o valor do boleto
	 * pagamentoBoleto.setAutenticacao() recebe o m�todo gerarAutenticacaoDePagamento(35) logo abaixo
	 * esse m�todo gera uma string alfanumerica de caracteres
	 * ao final ser� retornado o objeto pagamentoBoleto com as informa��es do pagamento
	 *  
	 * @param data_pagamento_boleto
	 * @param hora_pagamento_boleto
	 */
	public PagamentoImpostos salvarPagamentoImposto(TituloPagamento titulo, TipoCliente tipoCliente) {
				
		
		String dataPagAtual = pagamentoImpostos.gerarDataDePagamentoAtual();
		String horaPagAtual = pagamentoImpostos.gerarHoraDePagamentoAtual();
		double saldoAtualizado;
		
		if(titulo.getValor() <= tipoCliente.getSaldo()) {
			pagamentoImpostos.setId(titulo.getIdTitulo());
			pagamentoImpostos.setHora_pagamento(horaPagAtual);
			pagamentoImpostos.setNomeRecebedor(titulo.getBeneficiario());
			pagamentoImpostos.setCpfOuCnpjPagador(tipoCliente.getIdentificacao());
			pagamentoImpostos.setValor(titulo.getValor());
			pagamentoImpostos.setData_pagamento(dataPagAtual);
			pagamentoImpostos.setNomePagador(tipoCliente.getNome());
			pagamentoImpostos.setAutenticacao(gerarAutenticacaoDePagamento(35));
			
			saldoAtualizado = tipoCliente.getSaldo() - titulo.getValor();
			tipoCliente.setSaldo(saldoAtualizado);
		}
		
		return pagamentoImpostos;
	}

	
/**
 * Esse m�todo recebe um parametro que ser� o tamanho da nossa "autenticacao"
 * ela ter� somente os caracteres que est�o em theAlphaNumericS
 * depois um for far� o sorteio dos caracteres e guarda na variavel builder
 * no final retornamos a variavel builder formatando para toString
 * 
 * @param i
 * @return
 */
public String gerarAutenticacaoDePagamento(int i) { 
    String theAlphaNumericS;
    StringBuilder builder;
    
    theAlphaNumericS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                + "0123456789"; 

    //create the StringBuffer
    builder = new StringBuilder(i); 

    for (int m = 0; m < i; m++) { 

        // generate numeric
        int myindex 
            = (int)(theAlphaNumericS.length() 
                    * Math.random()); 

        // add the characters
        builder.append(theAlphaNumericS 
                    .charAt(myindex)); 
    } 

    return builder.toString(); 
}
	

	
	/**
	 * 
	 * @param vencimentoBoleto
	 * @return
	 */
	public double compararDatasPagamentoComVencimentoIptu(String vencimentoBoleto, 
			double impostoInterface , double valorImposto, double valorSaldo) {
		
		double valorBoleto = pagamentoImpostos.getValor();
		
		if (pagamentoImpostos.getData_pagamento().compareTo(vencimentoBoleto) == 0) {
			
			System.out.println("Vencimento igual a data de pagamento");
			
			valorSaldo -= valorBoleto;
			
		
		}else if(pagamentoImpostos.getData_pagamento().compareTo(vencimentoBoleto) < 0) {
			System.out.println("Pagamento antes do vencimento");
			
			valorSaldo -= valorBoleto;
		} 
			
		else if(pagamentoImpostos.getData_pagamento().compareTo(vencimentoBoleto) > 0){
			
			System.out.println("Pagamento depois do vencimento");
		
			valorBoleto += ((valorImposto * impostoInterface) / 100); 
			
			valorSaldo -= valorBoleto;
		}
		
			System.out.println(valorBoleto);
			System.out.println("Saldo em conta "+valorSaldo);
			
			pagamentoImpostos.setValor(valorBoleto);
			
			return pagamentoImpostos.getValor();
	}



}