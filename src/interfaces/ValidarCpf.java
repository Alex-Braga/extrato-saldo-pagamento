package interfaces;

import model.ClientePF;


public interface ValidarCpf {
	
	
	/**
	 * M�todo que verifica a validade do CPF 
	 * @param cpf
	 * @return
	 */
 	public boolean validaCpf (String cpf) ;
}
