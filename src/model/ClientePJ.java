package model;

import interfaces.TipoCliente;

public class ClientePJ extends Cliente implements TipoCliente {
	
	public ClientePJ(int id, String nome, String chavePix, double saldo, String cnpj) {
		super(id, nome, chavePix, saldo);
		// TODO Auto-generated constructor stub
	}
	
	public ClientePJ() {}

	private String cnpj;

	@Override
	public String getIdentificacao() {
		return cnpj;
	}

	@Override
	public void setIdentificacao(String cnpj) {
		this.cnpj = cnpj;
	}
	
}
