/**
 * 
 */
package controller;

import model.Extrato;
import model.Pagamento;
import model.PagamentoBoleto;
import model.PagamentoImpostos;
import model.PagamentoPix;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.text.ParseException;


/**
 * @author dan
 *
 */
public class ExtratoController {
	
	Extrato extrato;
	
	ArrayList<PagamentoBoleto> arrayPagamentoBoleto = new ArrayList<PagamentoBoleto>();
	ArrayList<PagamentoImpostos> arrayPagamentoImpostos = new ArrayList<PagamentoImpostos>();
	ArrayList<PagamentoPix> arrayPagamentoPix = new ArrayList<PagamentoPix>();

	
	/**
	 * 
	 * @return
	 */
	public ArrayList<PagamentoBoleto> listarPagamentosBoleto() {
		
		for(int i = 0; i <arrayPagamentoBoleto.size(); i++) {
			System.out.println(arrayPagamentoBoleto.get(i).getAutenticacao());
		}
		
		return arrayPagamentoBoleto;
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<PagamentoImpostos> listarPagamentosImpostos() {
		
		for(int i = 0; i <arrayPagamentoImpostos.size(); i++) {
			System.out.println(arrayPagamentoImpostos.get(i).getAutenticacao());
		}
		
		return arrayPagamentoImpostos;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<PagamentoPix> listarPagamentoPix() {
		
		for(int i = 0; i <arrayPagamentoPix.size(); i++) {
			System.out.println(arrayPagamentoPix.get(i).getAutenticacao());
		}
		
		return arrayPagamentoPix;
	}
	
	
	/**
	 * 
	 * @param pagamentoBoleto
	 */
	public void lancarPagamentoBoleto(PagamentoBoleto pagamentoBoleto) {
		if(pagamentoBoleto.getAutenticacao() != null) {
			
			arrayPagamentoBoleto.add(pagamentoBoleto);
			}
	}
		
		
	/**
	 * 
	 * @param pagamentoImpostos
	 */
	public void lancarPagamentoImpostos(PagamentoImpostos pagamentoImpostos) {
		if(pagamentoImpostos.getAutenticacao() != null) {
		arrayPagamentoImpostos.add(pagamentoImpostos);
		}
	}
	
	/**
	 * 
	 * @param pagamentoPix
	 */
	public void lancarPagamentoPix(PagamentoImpostos pagamentoPix) {
		if(pagamentoPix.getAutenticacao() != null) {
		arrayPagamentoImpostos.add(pagamentoPix);
		}
	}

}
