package interfaces;

public interface TituloPagamento {
	
	public double getValor();
	public String getBeneficiario();
	public int getIdTitulo();
	
	
}
