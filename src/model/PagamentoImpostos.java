package model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import interfaces.TipoImposto;



public class PagamentoImpostos extends Pagamento implements TipoImposto{
	
	TipoImposto tipoImposto;

	public PagamentoImpostos(int id, LocalDateTime data, String nomePagador, 
			String cpfOuCnpjPagador, String nomeRecebedor,
			String cpfOuCnpjRecebedor, double valor, String data_pagamento, 
			String hora_pagamento,
			String autenticacao, TipoImposto tipoimposto) {
		super(id, data, nomePagador, cpfOuCnpjPagador, nomeRecebedor, 
				cpfOuCnpjRecebedor, valor, data_pagamento, hora_pagamento,
				autenticacao);
		// TODO Auto-generated constructor stub
	}
	
	public PagamentoImpostos() {
	
	}
		
	
	public TipoImposto getTipoImposto() {
		return tipoImposto;
	}

	public void setTipoImposto(TipoImposto tipoImposto) {
		this.tipoImposto = tipoImposto;
	}

	
	
	
	
	
}
