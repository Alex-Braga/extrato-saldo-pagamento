package controller;

import interfaces.ValidarCnpj;
import interfaces.ValidarCpf;
import model.Cliente;
import model.ClientePF;
import model.ClientePJ;


public class ClienteController implements ValidarCpf,ValidarCnpj{
	
	Cliente cliente;
	ClientePJ pj; 
	ClientePF pf; 
	
	/**
	 * Verifica se cliente possui saldo suficiente para efetuar pagamento.
	 * @param valorOperacao
	 * @return boolean (true = saldo suficiente)
	 */
	public boolean verificarSaldoSuficienteParaPagamentoPessoaFisica(double valorOperacao) {
		if(pf.getSaldo() >= valorOperacao) {
			return true;
		}
		return false;
	}
	
	/**
	 * Verifica se cliente possui saldo suficiente para efetuar pagamento.
	 * @param valorOperacao
	 * @return boolean (true = saldo suficiente)
	 */
	public boolean verificarSaldoSuficienteParaPagamentoPessoaJuridica(double valorOperacao) {
		if(pj.getSaldo() >= valorOperacao) {
			return true;
		}
		return false;
	}

	/**
	 * Verifica se a chave pix inserida � v�lida.
	 * @param chavePix
	 * @return boolean (true = chave v�lida)
	 */
	public boolean validaChave(String chavePix) {
		if (chavePix == cliente.getChavePix()) {
			return true;
		}
		return false;
		
	}

	@Override
	public boolean validaCnpj(String cnpj) {
		cnpj = pj.getIdentificacao();
		
		if(cnpj.matches("[0-9]*") ||
				cnpj.length() != 14 ||
		 		   cnpj.isBlank() ) {
		 			return true; 
		 		}
		return false;
	}

	@Override
	public boolean validaCpf(String cpf) {
		cpf = pf.getIdentificacao();
	 		if(cpf.matches("[0-9]*") ||
	 		   cpf.length() != 11 ||
	 		   cpf.isBlank() ) {
	 			return true; 
	 		}
	 	return false;
	}
}

