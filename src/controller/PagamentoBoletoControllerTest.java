package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import model.Boleto;
import model.Cliente;
import model.ClientePF;
import model.PagamentoBoleto;

class PagamentoBoletoControllerTest {
	
	@Test
	void salvarPagamentoDeBoletoComSaldoDisponivel() {
		PagamentoBoleto pagamentoBoleto = new PagamentoBoleto();
		PagamentoBoletoController pagamentoBoletoController = new PagamentoBoletoController();
		pagamentoBoletoController.pagamentoBoleto = pagamentoBoleto;
		
		ClientePF cliente = new ClientePF(2, "Diego", "12199022100", 2000, "12199022100");
		Boleto boleto = new Boleto(90, "55555555555", 1000, "27/06/2021", "26/06/2021", "10:00", "Kabum", 
								   "Compra Online", "1091892000199", "12311", "10928881-1", "pag-boleto");
		
		pagamentoBoletoController.salvarPagamentoDeBoleto(boleto, cliente);
		Assert.assertNotNull(pagamentoBoletoController.pagamentoBoleto.getAutenticacao());
	}
	
	@Test
	void salvarPagamentoDeBoletoSemSaldoDisponivel() {
		PagamentoBoleto pagamentoBoleto = new PagamentoBoleto();
		PagamentoBoletoController pagamentoBoletoController = new PagamentoBoletoController();
		pagamentoBoletoController.pagamentoBoleto = pagamentoBoleto;
		
		ClientePF cliente = new ClientePF(2, "Diego", "12199022100", 1000, "12199022100");
		Boleto boleto = new Boleto(90, "55555555555", 2000, "27/06/2021", "26/06/2021", "10:00", "Kabum", 
								   "Compra Online", "1091892000199", "12311", "10928881-1", "pag-boleto");
		
		pagamentoBoletoController.salvarPagamentoDeBoleto(boleto, cliente);
		Assert.assertNull(pagamentoBoletoController.pagamentoBoleto.getAutenticacao());
	}
	
	@Test
	void gerarHoraDePagamentoAtual() {
		PagamentoBoleto pagamentoBoleto = new PagamentoBoleto();
		PagamentoBoletoController pagamentoBoletoController = new PagamentoBoletoController();
		pagamentoBoletoController.pagamentoBoleto = pagamentoBoleto;
		pagamentoBoletoController.pagamentoBoleto.gerarHoraDePagamentoAtual();
		Assert.assertNotNull(pagamentoBoletoController.pagamentoBoleto.getHora_pagamento());
	}
	
	@Test
	void gerarDataDePagamentoAtual() {
		PagamentoBoleto pagamentoBoleto = new PagamentoBoleto();
		PagamentoBoletoController pagamentoBoletoController = new PagamentoBoletoController();
		pagamentoBoletoController.pagamentoBoleto = pagamentoBoleto;
		pagamentoBoletoController.pagamentoBoleto.gerarDataDePagamentoAtual();
		Assert.assertNotNull(pagamentoBoletoController.pagamentoBoleto.gerarDataDePagamentoAtual());
	}
	
	@Test
	void compararDataPagamentoDepoisDoVencimento() {
		PagamentoBoleto pagamentoBoleto = new PagamentoBoleto();
		PagamentoBoletoController pagamentoBoletoController = new PagamentoBoletoController();
		pagamentoBoletoController.pagamentoBoleto = pagamentoBoleto;
		Boleto boleto = new Boleto();
		boleto.setData_vencimento("22/08/2021");
		boleto.setValor(200);
		pagamentoBoletoController.pagamentoBoleto.setValor(200);
		pagamentoBoletoController.pagamentoBoleto.gerarDataDePagamentoAtual();
		pagamentoBoletoController.pagamentoBoleto.compararDatasPagamentoComVencimento(boleto.getData_vencimento());
		Assert.assertNotEquals(boleto.getValor(), pagamentoBoletoController.pagamentoBoleto.getValor(), 0000.9);
	}
	
	@Test
	void compararDataPagamentoNoDiaDoVencimento() {
		PagamentoBoleto pagamentoBoleto = new PagamentoBoleto();
		PagamentoBoletoController pagamentoBoletoController = new PagamentoBoletoController();
		pagamentoBoletoController.pagamentoBoleto = pagamentoBoleto;
		Boleto boleto = new Boleto();
		boleto.setData_vencimento("30/08/2021");
		boleto.setValor(200);
		pagamentoBoletoController.pagamentoBoleto.setValor(200);
		pagamentoBoletoController.pagamentoBoleto.gerarDataDePagamentoAtual();
		pagamentoBoletoController.pagamentoBoleto.compararDatasPagamentoComVencimento(boleto.getData_vencimento());
		Assert.assertEquals(boleto.getValor(), pagamentoBoletoController.pagamentoBoleto.getValor(), 0000.9);
	}
	
	@Test
	void compararDataPagamentoAntesDoVencimento() {
		PagamentoBoleto pagamentoBoleto = new PagamentoBoleto();
		PagamentoBoletoController pagamentoBoletoController = new PagamentoBoletoController();
		pagamentoBoletoController.pagamentoBoleto = pagamentoBoleto;
		Boleto boleto = new Boleto();
		boleto.setData_vencimento("31/08/2021");
		boleto.setValor(400);
		pagamentoBoletoController.pagamentoBoleto.setValor(400);
		pagamentoBoletoController.pagamentoBoleto.gerarDataDePagamentoAtual();
		pagamentoBoletoController.pagamentoBoleto.compararDatasPagamentoComVencimento(boleto.getData_vencimento());
		Assert.assertEquals(boleto.getValor(), pagamentoBoletoController.pagamentoBoleto.getValor(), 0000.9);
	}

}