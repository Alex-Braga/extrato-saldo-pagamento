package model;

import interfaces.TituloPagamento;

public class Boleto implements TituloPagamento{
	
	private int id_boleto;
	private String cod_barras;
	private double valor;
	private String data_vencimento;
	private String data_pagamento;
	private String hora_pagamento;
	private String beneficiario;
	private String servico;
	private String CNPJ;
	private String agencia;
	private String conta;
	private String operacao;
	
	/**
	 * m�todo que retorna o id
	 * @return
	 */
	public int getid_boleto() {
		return id_boleto;
	}

	/**
	 * m�todo para inserir valor do id
	 * @param id_boleto
	 */
	public void setid_boleto(int id_boleto) {
		this.id_boleto = id_boleto;
	}

	/**
	 * m�todo que retorna o c�digo de barras
	 * @return
	 */
	public String getCod_barras() {
		return cod_barras;
	}

	/**
	 * m�todo para inserir o c�digo de barras
	 * @param cod_barras
	 */
	public void setCod_barras(String cod_barras) {
		this.cod_barras = cod_barras;
	}

	/**
	 * m�todo que retorna o valor do boletp
	 * @return
	 */
	public double getValor() {
		return valor;
	}

	/**
	 * m�todo para inserir o valor ao boleto
	 * @param valor
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}

	/**
	 * m�todo que retorna a data de vencimento
	 * @return
	 */
	public String getData_vencimento() {
		return data_vencimento;
	}

	/**
	 * m�todo que insere o valor na data de vencimento
	 * @param data_vencimento
	 */
	public void setData_vencimento(String data_vencimento) {
		this.data_vencimento = data_vencimento;
	}

	/**
	 * m�todo que retorna a data de vencimento
	 * @return
	 */
	public String getData_pagamento() {
		return data_pagamento;
	}

	/**
	 * m�todo para inserir a data de pagamento
	 * @param data_pagamento
	 */
	public void setData_pagamento(String data_pagamento) {
		this.data_pagamento = data_pagamento;
	}

	/**
	 * m�todo que retorna a hora do pagamento
	 * @return
	 */
	public String getHora_pagamento() {
		return hora_pagamento;
	}

	/**
	 * m�todo para inserir a hora de pagamento
	 * @param data_pagamento
	 */
	public void setHora_pagamento(String hora_pagamento) {
		this.hora_pagamento = hora_pagamento;
	}

	/**
	 * m�todo que retorna o nome do beneficiario
	 * @return
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * m�todo para inserir o nome do beneficiario
	 * @param data_pagamento
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * m�todo que retorna o tipo do servico
	 * @return
	 */
	public String getServico() {
		return servico;
	}

	/**
	 * m�todo para inserir o tipo de servi�o
	 * @param data_pagamento
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}

	/**
	 * m�todo que retorna o numero do CNPJ
	 * @return
	 */
	public String getCNPJ() {
		return CNPJ;
	}

	/**
	 * m�todo para inserir um valor ao CNPJ
	 * @param data_pagamento
	 */
	public void setCNPJ(String cNPJ) {
		CNPJ = cNPJ;
	}

	/**
	 * m�todo que retorna o numero da agencia
	 * @return
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * m�todo para inserir o numero da agencia
	 * @param data_pagamento
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * m�todo que retorna o numero da conta
	 * @return
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * m�todo para inserir o numero da conta
	 * @param data_pagamento
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * m�todo que retorna o tipo de operacao
	 * @return
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * m�todo para inserir o tipo de opera��o
	 * @param data_pagamento
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	
	public Boleto() {
		
	}

	public Boleto(int id_boleto, String cod_barras, double valor, 
				  String data_vencimento, String data_pagamento, 
				  String hora_pagamento, String beneficiario, 
				  String servico, String CNPJ, String agencia, 
				  String conta, String operacao) {
		
		this.id_boleto = id_boleto;
		this.cod_barras = cod_barras;
		this.valor = valor;
		this.data_vencimento = data_vencimento;
		this.data_pagamento = data_pagamento;
		this.hora_pagamento = hora_pagamento;
		this.beneficiario = beneficiario;
		this.servico = servico;
		this.CNPJ = CNPJ;
		this.agencia = agencia;
		this.conta = conta;
		this.operacao = operacao;
		
	}

	@Override
	public int getIdTitulo() {
		return id_boleto;
	}
	
	

}
