package controller;

import interfaces.BuscarBoleto;
import model.Boleto;

public class BoletoController implements BuscarBoleto {

	Boleto boleto;

	/**
	 * m�todo que retorna o valor do boleto de acordo com 
	 * o c�digo de barras passado por param�tro
	 * 
	 * @param cod_barras
	 * @return
	 */
	@Override
	public Boleto buscarBoleto(String cod_barras) {
		if (boleto.getCod_barras() == cod_barras) return boleto; 
		else return null;
	}
	 
}
