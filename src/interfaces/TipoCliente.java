package interfaces;

public interface TipoCliente {
	
	public int getId();
	public String getNome();
	public String getChavePix();
	public double getSaldo();
	public String getIdentificacao();
	public void setIdentificacao(String identificacao);
	public void setSaldo(double saldo);
	
}
