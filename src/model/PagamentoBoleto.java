package model;

import java.time.LocalDateTime;

public class PagamentoBoleto extends Pagamento{

	public PagamentoBoleto(int id, LocalDateTime data, String nomePagador, String cpfOuCnpjPagador,
			String nomeRecebedor, String cpfOuCnpjRecebedor, double valor, String data_pagamento, String hora_pagamento,
			String autenticacao) {
		super(id, data, nomePagador, cpfOuCnpjPagador, nomeRecebedor, cpfOuCnpjRecebedor, valor, data_pagamento, hora_pagamento,
				autenticacao);
		// TODO Auto-generated constructor stub
	}
	
	public PagamentoBoleto() {
		
	}
	

}
