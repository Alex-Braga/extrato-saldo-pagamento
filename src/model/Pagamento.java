package model;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;


public class Pagamento {
	
	protected int id;
	protected LocalDateTime data;
	protected String nomePagador;
	protected String cpfOuCnpjPagador;
	protected String nomeRecebedor;
	protected String cpfOuCnpjRecebedor;
	protected double valor;
	protected String data_pagamento;
	protected String hora_pagamento;
	protected String autenticacao;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public LocalDateTime getData() {
		return data;
	}


	public void setData(LocalDateTime data) {
		this.data = data;
	}


	public String getNomePagador() {
		return nomePagador;
	}


	public void setNomePagador(String nomePagador) {
		this.nomePagador = nomePagador;
	}


	public String getCpfOuCnpjPagador() {
		return cpfOuCnpjPagador;
	}


	public void setCpfOuCnpjPagador(String cpfOuCnpjPagador) {
		this.cpfOuCnpjPagador = cpfOuCnpjPagador;
	}


	public String getNomeRecebedor() {
		return nomeRecebedor;
	}


	public void setNomeRecebedor(String nomeRecebedor) {
		this.nomeRecebedor = nomeRecebedor;
	}


	public String getCpfOuCnpjRecebedor() {
		return cpfOuCnpjRecebedor;
	}


	public void setCpfOuCnpjRecebedor(String cpfOuCnpjRecebedor) {
		this.cpfOuCnpjRecebedor = cpfOuCnpjRecebedor;
	}


	public double getValor() {
		return valor;
	}


	public void setValor(double valor) {
		this.valor = valor;
	}


	public String getData_pagamento() {
		return data_pagamento;
	}
 

	public void setData_pagamento(String data_pagamento) {
		this.data_pagamento = data_pagamento;
	}


	public String getHora_pagamento() {
		return hora_pagamento;
	}


	public void setHora_pagamento(String hora_pagamento) {
		this.hora_pagamento = hora_pagamento;
	}


	public String getAutenticacao() {
		return autenticacao;
	}


	public void setAutenticacao(String autenticacao) {
		this.autenticacao = autenticacao;
	}

	
	

	public Pagamento(int id, LocalDateTime data, String nomePagador, String cpfOuCnpjPagador, String nomeRecebedor,
			String cpfOuCnpjRecebedor, double valor, String data_pagamento, String hora_pagamento,
			String autenticacao) {
		
		this.id = id;
		this.data = data;
		this.nomePagador = nomePagador;
		this.cpfOuCnpjPagador = cpfOuCnpjPagador;
		this.nomeRecebedor = nomeRecebedor;
		this.cpfOuCnpjRecebedor = cpfOuCnpjRecebedor;
		this.valor = valor;
		this.data_pagamento = data_pagamento;
		this.hora_pagamento = hora_pagamento;
		this.autenticacao = autenticacao;
	}
	
	public Pagamento() {}
	
	/**
	 * m�todo para pegar a data atual com LocalDate e passar essa data para
	 * this.setData_pagamento, assim quando o cliente for pagar, vai pegar o dia
	 * atual e passar para o objeto pagamento boleto, pix ou impostos
	 * 
	 * @return this.getData_pagamento()
	 */
	public String gerarDataDePagamentoAtual() {
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		this.setData_pagamento(localDate.format(formatter));
		return this.getData_pagamento();
	}
	
	/**
	 * m�todo para pegar a hora atual com Calendar e passar essa data para
	 * this.setHora_pagamento
	 * 
	 * @return this.getHora_pagamento()
	 */
	public String gerarHoraDePagamentoAtual() {
		Calendar data = Calendar.getInstance();
		int horas = data.get(Calendar.HOUR_OF_DAY);
		int minutos = data.get(Calendar.MINUTE);
		int segundos = data.get(Calendar.SECOND);
		
		String h = Integer.toString(horas);
		String m = Integer.toString(minutos);
		String s = Integer.toString(segundos);
		
		this.setHora_pagamento(h+":"+m+":"+s);
		
		return this.getHora_pagamento();
	}
	
	/**
	 * m�todo que recebe o vencimento do boleto e compara se ele � igual
	 * ao dia de pagamento 0, anterior ao dia de vencimento -1, ou posterior ao
	 * dia de vencimento +1
	 * 
	 * caso o pagamento seja posterior ao vencimento, �ltimo else, ent�o
	 * o valor do pagamento vai ter juros de 5% acrescido do valor
	 * 
	 * @param vencimentoBoleto
	 * @return this.getValor();
	 */
	public double compararDatasPagamentoComVencimento(String vencimentoBoleto) {
		double valorBoleto = this.getValor();
		if (this.getData_pagamento().compareTo(vencimentoBoleto) == 0) {
			System.out.println("Vencimento igual a data de pagamento");
		} else if(this.getData_pagamento().compareTo(vencimentoBoleto) < 0){
			System.out.println("Pagamento antes do vencimento");
		} else if(this.getData_pagamento().compareTo(vencimentoBoleto) > 0){
			System.out.println("Pagamento depois do vencimento");
			valorBoleto = valorBoleto + (valorBoleto * 0.05);
		}
		System.out.println(valorBoleto);
		this.setValor(valorBoleto);	
		return this.getValor();
	}
	
}

























