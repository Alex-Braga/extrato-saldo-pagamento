package controller;

import interfaces.TipoCliente;
import interfaces.TituloPagamento;
import interfaces.ValidarCnpj;
import interfaces.ValidarCpf;
import model.Cliente;
import model.ClientePF;
import model.PagamentoBoleto;
import model.PagamentoPix;

public class PagamentoPixController implements ValidarCpf, ValidarCnpj{

	
	PagamentoPix pagamentoPix; 
	
	/**
	 * M�todo que verifica a validade do CPF 
	 * @param cpf
	 * @return
	 */
	
			
	 	public boolean validaCpf (PagamentoPix cpf) {
	 		if(cpf.getCpfOuCnpjPagador().matches("[0-9]*") ||
	 		   cpf.getCpfOuCnpjPagador().length() != 11 ||
	 		   cpf.getCpfOuCnpjPagador().isBlank() ) {
	 			return true; 
	 		}
			return false;
	 		}
	 /**
	   * M�todo que verifica a validade do CNPJ 
	   * @param cnpj
	   * @return
	   */
	 	public boolean validaCnpj (PagamentoPix cnpj) {
	 		if(cnpj.getCpfOuCnpjPagador().matches("[0-9]*") ||
	 		   cnpj.getCpfOuCnpjPagador().length() != 14 ||
	 		   cnpj.getCpfOuCnpjPagador().isBlank() ) {
	 			return true; 
	 		}
			return false;
	 		}
	 	

		/**
		 * M�todo que verifica se o telefone � valido
		 * 
		 * @param telefone
		 * @return
		 */
		public static boolean telefone(String telefone) {
			return telefone.matches("(\\(?\\d{2}\\)?\\s)?(\\d{4,5}\\-\\d{4})");
		}
	
		
		
		
		
		public PagamentoPix salvarPagamentoDePix( double valorOp, TipoCliente tipoCliente) {
			
			String dataPagAtual = pagamentoPix.gerarDataDePagamentoAtual();
			String horaPagAtual = pagamentoPix.gerarHoraDePagamentoAtual();
			double saldoAtualizado;
			
			if(valorOp <= tipoCliente.getSaldo()) {
				pagamentoPix.setHora_pagamento(horaPagAtual);
				pagamentoPix.setCpfOuCnpjPagador(tipoCliente.getIdentificacao());
				pagamentoPix.setValor(valorOp);
				pagamentoPix.setData_pagamento(dataPagAtual);
				pagamentoPix.setNomePagador(tipoCliente.getNome());
				pagamentoPix.setChaveCliente(tipoCliente.getChavePix());
				pagamentoPix.setAutenticacao(gerarAutenticacaoDePagamento(35));
				
				saldoAtualizado = tipoCliente.getSaldo() - valorOp;
				tipoCliente.setSaldo(saldoAtualizado);
			}
			
			return pagamentoPix;
		}
		
		/**
		 * Esse m�todo recebe um parametro que ser� o tamanho da nossa "autenticacao"
		 * ela ter� somente os caracteres que est�o em theAlphaNumericS
		 * depois um for far� o sorteio dos caracteres e guarda na variavel builder
		 * no final retornamos a variavel builder formatando para toString
		 * 
		 * @param i
		 * @return
		 */
		public String gerarAutenticacaoDePagamento(int i) { 
	        String theAlphaNumericS;
	        StringBuilder builder;
	        
	        theAlphaNumericS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	                                    + "0123456789"; 

	        //create the StringBuffer
	        builder = new StringBuilder(i); 

	        for (int m = 0; m < i; m++) { 

	            // generate numeric
	            int myindex 
	                = (int)(theAlphaNumericS.length() 
	                        * Math.random()); 

	            // add the characters
	            builder.append(theAlphaNumericS 
	                        .charAt(myindex)); 
	        } 

	        return builder.toString(); 
	    }
		
		
		@Override
		public boolean validaCnpj(String cnpj) {
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public boolean validaCpf(String cpf) {
			// TODO Auto-generated method stub
			return false;
		}
		;
}


