package interfaces;

import model.Boleto;

public interface BuscarBoleto {
	
	public Boleto buscarBoleto(String cod_barras); 

}
