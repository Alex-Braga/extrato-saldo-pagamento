package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import model.Cliente;
import model.ClientePF;
import model.PagamentoPix;

class PagamentoPixControllerTest {

	@Test
	void testPagamentoPixComprovante() {
		ClientePF clientePf = new ClientePF();
		PagamentoPix pagamentoPix = new PagamentoPix();
		PagamentoPixController pagamentoPixController = new PagamentoPixController();
		pagamentoPixController.pagamentoPix = pagamentoPix;
		clientePf.setChavePix("123456ab");
		clientePf.setSaldo(1200);
		pagamentoPixController.pagamentoPix.setValor(100);
		pagamentoPixController.salvarPagamentoDePix(pagamentoPix.getValor(),clientePf);
		
		Assert.assertNotNull(pagamentoPixController.pagamentoPix.getAutenticacao());
		}
	
	
	@Test 
	void testValidaCpfTrue() {
		PagamentoPix pagamentoPix  = new PagamentoPix();
		PagamentoPixController pagamentoPixController = new PagamentoPixController();
		pagamentoPix.setCpfOuCnpjPagador("12345678912");
		Assert.assertTrue (pagamentoPixController.validaCpf(pagamentoPix.getCpfOuCnpjPagador()));
		
		
	}
	
	
	
	
	
	
			//Teste valida��o CPF
			//Set cpf, refazer testes
//			@Test
//			void validaCPFRetornaTrue() {
//				Assert.assertTrue(PagamentoPixController.validaCpf("02775321321"));
//			}
//			
//			@Test
//			void validaCPFRetornaFalseIsBlank() {
//				Assert.assertFalse(PagamentoPixController.validaCPF(""));
//			}
//			
//			@Test
//			void validaCPFRetornaFalseLetras() {
//				Assert.assertFalse(PagamentoPixController.validaCPF("abcdefghijk"));
//			}
//			
//			@Test
//			void validaCPFRetornaFalseMaiorQue11Digitos() {
//				Assert.assertFalse(PagamentoPixController.validaCPF("123456789101"));
//			}
//			
//			@Test
//			void validaCPFRetornaFalseMenorQue11Digitos() {
//				Assert.assertFalse(PagamentoPixController.validaCPF("1"));
//			}
//			
//			@Test
//			void validaCPFRetornaFalseLetrasENumeros() {
//				Assert.assertFalse(PagamentoPixController.validaCPF("1234567891a"));
//			}
//			
//			//Teste valida��o CNPJ
//			@Test
//			void validateCNPJRetornaTrue() {
//				Assert.assertTrue(PagamentoPixController.validaCNPJ("02775321321123"));
//			}
//			
//			@Test
//			void validaCNPJRetornaFalseIsBlank() {
//				Assert.assertFalse(PagamentoPixController.validaCNPJ(""));
//			}
//			
//			@Test
//			void validaCNPJRetornaFalseLetras() {
//				Assert.assertFalse(PagamentoPixController.validaCNPJ("abcdefghijklmn"));
//			}
//			
//			@Test
//			void validaCNPJRetornaFalseMaiorQue14Digitos() {
//				Assert.assertFalse(PagamentoPixController.validaCNPJ("123456789101234"));
//			}
//			
//			@Test
//			void validaCNPJRetornaFalseMenorQue14Digitos() {
//				Assert.assertFalse(PagamentoPixController.validaCNPJ("1"));
//			}
//			
//			@Test
//			void validaCNPJRetornaFalseLetrasENumeros() {
//				Assert.assertFalse(PagamentoPixController.validaCNPJ("12c34567b891a2"));
//			}
//			
//			//Verifica se o n�mero de telefone � v�lido 
//			
//			@Test
//			void telefoneValido() {
//				assertTrue(PagamentoPixController.telefone("99999-9999"));
//			}
//			@Test
//			void telefoneInvalido() {
//				assertFalse(PagamentoPixController.telefone("999999999"));
//			}
//			@Test
//			void telefoneValidoDDD() {
//				assertTrue(PagamentoPixController.telefone("(71) 99999-9999"));
//			}
//			@Test
//			void telefoneInvalidoDDD() {
//				assertFalse(PagamentoPixController.telefone("(71)99999-9999"));
//			}

}
