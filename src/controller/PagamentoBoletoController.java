package controller;

import model.Boleto;
import model.Cliente;
import model.ClientePF;
import model.Pagamento;
import model.PagamentoBoleto;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;

import interfaces.TipoCliente;
import interfaces.TituloPagamento;

public class PagamentoBoletoController {
	
	PagamentoBoleto pagamentoBoleto;
	
	/**
	 * m�todo para receber informa��es do boleto e informa��es do cliente
	 * os dados do pagamento somente ser�o salvos se o saldo for maior que o valor do boleto
	 * pagamentoBoleto.setAutenticacao() recebe o m�todo gerarAutenticacaoDePagamento(35) logo abaixo
	 * esse m�todo gera uma string alfanumerica de caracteres
	 * ao final ser� retornado o objeto pagamentoBoleto com as informa��es do pagamento
	 *  
	 * @param data_pagamento_boleto
	 * @param hora_pagamento_boleto
	 */
	public PagamentoBoleto salvarPagamentoDeBoleto(TituloPagamento titulo, TipoCliente tipoCliente) {
		
		String dataPagAtual = pagamentoBoleto.gerarDataDePagamentoAtual();
		String horaPagAtual = pagamentoBoleto.gerarHoraDePagamentoAtual();
		double saldoAtualizado;
		
		if(titulo.getValor() <= tipoCliente.getSaldo()) {
			pagamentoBoleto.setId(titulo.getIdTitulo());
			pagamentoBoleto.setHora_pagamento(horaPagAtual);
			pagamentoBoleto.setNomeRecebedor(titulo.getBeneficiario());
			pagamentoBoleto.setCpfOuCnpjPagador(tipoCliente.getIdentificacao());
			pagamentoBoleto.setValor(titulo.getValor());
			pagamentoBoleto.setData_pagamento(dataPagAtual);
			pagamentoBoleto.setNomePagador(tipoCliente.getNome());
			pagamentoBoleto.setAutenticacao(gerarAutenticacaoDePagamento(35));
			
			saldoAtualizado = tipoCliente.getSaldo() - titulo.getValor();
			tipoCliente.setSaldo(saldoAtualizado);
		}
		
		return pagamentoBoleto;
	}
	
	/**
	 * Esse m�todo recebe um parametro que ser� o tamanho da nossa "autenticacao"
	 * ela ter� somente os caracteres que est�o em theAlphaNumericS
	 * depois um for far� o sorteio dos caracteres e guarda na variavel builder
	 * no final retornamos a variavel builder formatando para toString
	 * 
	 * @param i
	 * @return
	 */
	public String gerarAutenticacaoDePagamento(int i) { 
        String theAlphaNumericS;
        StringBuilder builder;
        
        theAlphaNumericS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"; 

        //create the StringBuffer
        builder = new StringBuilder(i); 

        for (int m = 0; m < i; m++) { 

            // generate numeric
            int myindex 
                = (int)(theAlphaNumericS.length() 
                        * Math.random()); 

            // add the characters
            builder.append(theAlphaNumericS 
                        .charAt(myindex)); 
        } 

        return builder.toString(); 
    }
	
	
}