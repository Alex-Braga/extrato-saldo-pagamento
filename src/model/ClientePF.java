package model;

import interfaces.TipoCliente;

public class ClientePF extends Cliente implements TipoCliente {
	
	public ClientePF(int id, String nome, String chavePix, double saldo, String cpf) {
		super(id, nome, chavePix, saldo);
		// TODO Auto-generated constructor stub
	}
	
	public ClientePF() {}

	private String cpf;

	@Override
	public String getIdentificacao() {
		return cpf;
	}

	@Override
	public void setIdentificacao(String cpf) {
		this.cpf = cpf;
	}
	
	
}
