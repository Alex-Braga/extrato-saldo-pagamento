package controller;

import org.junit.Assert;


import org.junit.jupiter.api.Test;

import interfaces.*;
import model.Boleto;
import model.Cliente;
import model.ClientePF;
import model.Impostos;
import model.PagamentoBoleto;
import model.PagamentoImpostos;


class PagamentoImpostosControllerTest {	
	
	/**
	 * 
	 */
	@Test
	void confirmarPagamentoSaldoMaiorQueImpostoPF() {	
		PagamentoImpostos pagamentoImpostos = new PagamentoImpostos();
		PagamentoImpostosController controler = new PagamentoImpostosController();		
		ClientePF  clientePF = new ClientePF();
		
		controler.pagamentoImpostos = pagamentoImpostos;
		
		clientePF.setSaldo(100);
			
		controler.pagamentoImpostos.setValor(90);
		
		double valorFinal = controler.confirmarPagamento(clientePF.getSaldo(), 
				controler.pagamentoImpostos.getValor());
		Assert.assertEquals(10, valorFinal, 0);
	}
	
	/**
	 * 
	 */
	@Test
	void confirmarPagamentoSaldoMenorQueImpostoPF() {
		PagamentoImpostos pagamentoImpostos = new PagamentoImpostos();
		PagamentoImpostosController controler = new PagamentoImpostosController();
		ClientePF  clientePF = new ClientePF();
		
		controler.pagamentoImpostos = pagamentoImpostos;
		
		clientePF.setSaldo(100);
		
		controler.pagamentoImpostos.setValor(190);
		
		double valorFinal = controler.confirmarPagamento(clientePF.getSaldo(), 
				controler.pagamentoImpostos.getValor());
		
		Assert.assertEquals(0, valorFinal, 0);
	}
	
	
	/**
	 * 
	 */
	@Test
	void salvarPagamentoImpostoComSaldoDisponivelAntesDoVencimento() { 
		PagamentoImpostos pagamentoImpostos = new PagamentoImpostos();
		PagamentoImpostosController controler = new PagamentoImpostosController();
		
		controler.pagamentoImpostos = pagamentoImpostos;
		
		Impostos impostos = new Impostos();
		impostos.setValor(250.58);	
		
		ClientePF clientePF = new ClientePF();
		clientePF.setSaldo(1000);
		
		impostos.setData_vencimento("01/09/2021");
		impostos.setValor(790);
		
		double tipoImposto = TipoImposto.IPTU;
		
		controler.pagamentoImpostos.setValor(790);
		controler.pagamentoImpostos.gerarDataDePagamentoAtual();
		clientePF.setSaldo(2000);
		

		controler.compararDatasPagamentoComVencimentoIptu(impostos.getData_vencimento(), tipoImposto, 
				controler.pagamentoImpostos.getValor(), clientePF.getSaldo());
		
		controler.salvarPagamentoImposto(impostos, clientePF);
		
		Assert.assertNotNull(controler.pagamentoImpostos.getAutenticacao());

	}
	
	/**
	 * 
	 */
	@Test
	void salvarComprovanteDePagamentoImpostoSemSaldoDisponivel() { 
		PagamentoImpostos pagamentoImpostos = new PagamentoImpostos();
		PagamentoImpostosController controler = new PagamentoImpostosController();
		
		controler.pagamentoImpostos = pagamentoImpostos;
		
		Impostos impostos = new Impostos();
		impostos.setValor(2700);	
		
		ClientePF clientePF = new ClientePF();
		clientePF.setSaldo(1000);
		
		controler.salvarPagamentoImposto(impostos, clientePF);
		
		Assert.assertNull(controler.pagamentoImpostos.getAutenticacao());

	}
	
	@Test
	void compararDataPagamentoAntesDoVencimentoIptu() {
		PagamentoImpostos pagamentoImpostos = new PagamentoImpostos();
		PagamentoImpostosController controler = new PagamentoImpostosController();
		Impostos impostos = new Impostos();
		ClientePF clientePF = new ClientePF();
		 
		controler.pagamentoImpostos = pagamentoImpostos;
		
		impostos.setData_vencimento("30/09/2021");
		impostos.setValor(790);
		
		double tipoImposto = TipoImposto.IPTU;
		
		controler.pagamentoImpostos.setValor(790);
		controler.pagamentoImpostos.gerarDataDePagamentoAtual();
		clientePF.setSaldo(2000);
		

		controler.compararDatasPagamentoComVencimentoIptu(impostos.getData_vencimento(), tipoImposto, 
				controler.pagamentoImpostos.getValor(), clientePF.getSaldo());
		
		Assert.assertNotNull(controler.pagamentoImpostos.getValor());
	}
	
	@Test
	void compararDataPagamentoIgualDiaDoVencimentoIptu() {
		PagamentoImpostos pagamentoImpostos = new PagamentoImpostos();
		PagamentoImpostosController controler = new PagamentoImpostosController();
		Impostos impostos = new Impostos();
		ClientePF clientePF = new ClientePF();
		 
		controler.pagamentoImpostos = pagamentoImpostos;
		
		impostos.setData_vencimento("30/08/2021");
		impostos.setValor(500);
		
		double tipoImposto = TipoImposto.IPTU;
		
		controler.pagamentoImpostos.setValor(200);
		controler.pagamentoImpostos.gerarDataDePagamentoAtual();
		clientePF.setSaldo(780);
		

		controler.compararDatasPagamentoComVencimentoIptu(impostos.getData_vencimento(), tipoImposto, 
				controler.pagamentoImpostos.getValor(), clientePF.getSaldo());
		
		Assert.assertNotNull(controler.pagamentoImpostos.getValor());
	}
	

	/**
	 * Teste pagamento depois da data de pagamento, utilizando o a interface imposto
	 * basta chamar o tipo de imposto
	 */
	
	
	@Test
	void compararDataPagamentoDepoisDoVencimentoIptu() {
		PagamentoImpostos pagamentoImpostos = new PagamentoImpostos();
		PagamentoImpostosController controler = new PagamentoImpostosController();
		Impostos impostos = new Impostos();
		ClientePF clientePF = new ClientePF();	
		
		controler.pagamentoImpostos = pagamentoImpostos;
		
		impostos.setData_vencimento("01/08/2021");
		impostos.setValor(200);
		
		double tipoImposto = TipoImposto.IPTU;
		
		controler.pagamentoImpostos.setValor(200);
		controler.pagamentoImpostos.gerarDataDePagamentoAtual();
		clientePF.setSaldo(250);
		

		controler.compararDatasPagamentoComVencimentoIptu(impostos.getData_vencimento(), tipoImposto, 
				controler.pagamentoImpostos.getValor(), clientePF.getSaldo());
		
		Assert.assertNotNull(controler.pagamentoImpostos.getValor());
	} 
	
	
	
}


