package interfaces;

import model.ClientePJ;


public interface ValidarCnpj {
	
	
	/**
	   * M�todo que verifica a validade do CNPJ 
	   * @param cnpj
	   * @return
	   */
	 	public boolean validaCnpj (String cnpj);
	 	
}
