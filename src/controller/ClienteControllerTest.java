package controller;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import model.Boleto;
import model.Cliente;
import model.ClientePF;
import model.Pagamento;

class ClienteControllerTest {
	
	@Test
	void testaGettersAndSetters() {
		ClientePF clientePF = new ClientePF();
		ClienteController clienteController = new ClienteController();
		clienteController.cliente = clientePF;
		clienteController.cliente.setId(1);
		clienteController.cliente.setNome("Nome Sobrenome");
		clienteController.cliente.setCpf("12345678999");
		clienteController.cliente.setChavePix("12345678999");
		clienteController.cliente.setTipoChave("CPF");
		clienteController.cliente.setSaldo(5000);
		Assert.assertEquals(1, clienteController.cliente.getId());
		Assert.assertEquals("Nome Sobrenome", clienteController.cliente.getNome());
		Assert.assertEquals("12345678999", clienteController.cliente.getCpf());
		Assert.assertEquals("12345678999", clienteController.cliente.getChavePix());
		Assert.assertEquals("CPF", clienteController.cliente.getTipoChave());
		Assert.assertEquals(5000, clienteController.cliente.getSaldo(), 0);
	}

	@Test
	void saldoSuficienteRetornaTrue() {
		Cliente cliente = new Cliente();
		ClienteController clienteController = new ClienteController();
		clienteController.cliente = cliente;
		clienteController.cliente.setSaldo(100);
		Pagamento pagamento = new Pagamento();
		pagamento.setValor(80);
		double valorOperacao = pagamento.getValor();
		Assert.assertTrue(clienteController.verificarSaldoSuficienteParaPagamento(valorOperacao));		
	}
	
	@Test
	void saldoSuficienteRetornaFalse() {
		Cliente cliente = new Cliente();
		ClienteController clienteController = new ClienteController();
		clienteController.cliente = cliente;
		clienteController.cliente.setSaldo(100);
		Pagamento pagamento = new Pagamento();
		pagamento.setValor(180);
		double valorOperacao = pagamento.getValor();
		Assert.assertFalse(clienteController.verificarSaldoSuficienteParaPagamento(valorOperacao));		
	}
	
	@Test
	void validaChaveRetornaTrue() {
		Cliente cliente = new Cliente();
		ClienteController clienteController = new ClienteController();
		clienteController.cliente = cliente;
		clienteController.cliente.setChavePix("5573988645255");
		Assert.assertTrue(clienteController.validaChave("5573988645255")); //chave igual		
	}
	
	@Test
	void validaChaveRetornaFalse() {
		Cliente cliente = new Cliente();
		ClienteController clienteController = new ClienteController();
		clienteController.cliente = cliente;
		clienteController.cliente.setChavePix("5573988645255");
		Assert.assertFalse(clienteController.validaChave("5573900550025")); //chave diferente	
	}

}
