package interfaces;

public interface TipoImposto {
	
	final double IE    = 0.05;
	final double IRPJ  = 6.05;
	final double IRPF  = 8.07; 
	final double ITCMD = 0.06; 
	final double IPVA  = 0.08;
	final double TBI   = 4.04;
	final double ISS   = 0.03;
	final double IPTU  = 0.02;

	
}	
